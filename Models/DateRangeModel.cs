﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RF_Data_Importer.Models
{
    public class DateRangeModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
