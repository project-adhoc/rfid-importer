﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RF_Data_Importer.ViewModels;

namespace RF_Data_Importer.Models
{
    public class RFContext : DbContext
    {
        public RFContext(DbContextOptions<RFContext> options) : base(options)
        {
        }
        public DbSet<RFModel> RFData { get; set; }
        public DbSet<RF_Data_Importer.ViewModels.RFViewModel> RFViewModel { get; set; }
    }
}
