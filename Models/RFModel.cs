﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RF_Data_Importer.Models
{
    public class RFModel
    {
        [Key]
        public int Id { get; set; }
        public string AccessMessage { get; set; }
        public string Door { get; set; }
        public string Direction { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string CardNo { get; set; }
        public DateTime LocalTime { get; set; }
        public DateTime DateImported { get; set; }
    }
}
