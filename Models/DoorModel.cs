﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RF_Data_Importer.Models
{
    public class DoorModel
    {
        public int Id { get; set; }
        public string Door { get; set; }
    }
}
