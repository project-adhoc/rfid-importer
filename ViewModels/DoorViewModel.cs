﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RF_Data_Importer.ViewModels
{
    public class DoorViewModel
    {
        public int Id { get; set; }
        public string Door { get; set; }
        public string Message { get; set; }
    }
}
