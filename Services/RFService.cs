﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using RF_Data_Importer.Models;
using RF_Data_Importer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RF_Data_Importer.Services
{
    public class RFService : IRFService
    {
        private readonly RFContext _context;
        private readonly IConfiguration _configuration;
        public RFService(RFContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task Import(RFViewModel entity)
        {
            var isExists = await _context.RFData.AnyAsync(d => d.AccessMessage == entity.AccessMessage &&
            d.Door == entity.Door && d.Direction == entity.Direction && d.Username == entity.Username &&
            d.CardNo == entity.CardNo && d.LocalTime == entity.LocalTime);
            if (!isExists)
            {
                var model = new RFModel
                {
                    AccessMessage = entity.AccessMessage,
                    Door = entity.Door,
                    Direction = entity.Direction,
                    Fullname = entity.Fullname,
                    Username = entity.Username,
                    CardNo = entity.CardNo,
                    LocalTime = entity.LocalTime,
                    DateImported = entity.DateImported
                };
                _context.Add(model);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<RFViewModel>> Get()
        {
            var apiPath = _configuration["ApiPath"];
            var client = new RestClient(apiPath);
            var username = _configuration["BasicAuth:username"];
            var password = _configuration["BasicAuth:password"];
            client.Authenticator = new HttpBasicAuthenticator(username, password);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            var modelList = new List<RFViewModel>();
            var parsedJson = ParseJsonArray(response.Content);
            if (string.IsNullOrEmpty(parsedJson))
            { modelList = JsonConvert.DeserializeObject<List<RFViewModel>>(response.Content); }
            else
            {
                modelList.Add(new RFViewModel()
                { Message = parsedJson });
            }
            return await Task.FromResult(modelList);

            ////Task.Run(async () =>
            ////{
            ////    response = await GetResponseContentAsync(client, request) as RestResponse;
            ////}).Wait();
            //var result = JsonConvert.DeserializeObject<List<RFViewModel>>(response.Content);
            //return result;
        }

        /// <summary>
        /// rf/filter?datefrom=2020-01-11&dateto=2020-01-12
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public async Task<List<RFViewModel>> GetByDate(DateTime dateFrom, DateTime dateTo)
        {
            var apiPath = _configuration["ApiPath"];
            var url = $"{apiPath}/filter?datefrom={dateFrom}&dateto={dateTo}";
            var client = new RestClient(url);
            var username = _configuration["BasicAuth:username"];
            var password = _configuration["BasicAuth:password"];
            client.Authenticator = new HttpBasicAuthenticator(username, password);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            var modelList = new List<RFViewModel>();
            var parsedJson = ParseJsonArray(response.Content);
            if (string.IsNullOrEmpty(parsedJson))
            { modelList = JsonConvert.DeserializeObject<List<RFViewModel>>(response.Content); }
            else
            {
                modelList.Add(new RFViewModel()
                { Message = parsedJson });
            }
            return await Task.FromResult(modelList);
        }

        public async Task<List<DoorViewModel>> GetDoors()
        {
            var apiPath = _configuration["ApiPath"];
            var url = $"{apiPath}/doors";
            var client = new RestClient(url);
            var username = _configuration["BasicAuth:username"];
            var password = _configuration["BasicAuth:password"];
            client.Authenticator = new HttpBasicAuthenticator(username, password);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            var modelList = new List<DoorViewModel>();
            var parsedJson = ParseJsonArray(response.Content);
            if (string.IsNullOrEmpty(parsedJson))
            { modelList = JsonConvert.DeserializeObject<List<DoorViewModel>>(response.Content); }
            else
            {
                modelList.Add(new DoorViewModel()
                { Message = parsedJson });
            }
            return await Task.FromResult(modelList);
        }


        public Task ImportDoor(DoorViewModel entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<RFViewModel>> GetUserWithoutExit(DateTime dateFrom, DateTime dateTo)
        {
            var apiPath = _configuration["ApiPath"];
            var url = $"{apiPath}/UserWithoutExit?datefrom={dateFrom}&dateto={dateTo}";
            var client = new RestClient(url);
            var username = _configuration["BasicAuth:username"];
            var password = _configuration["BasicAuth:password"];
            client.Authenticator = new HttpBasicAuthenticator(username, password);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            var modelList = new List<RFViewModel>();
            var parsedJson = ParseJsonArray(response.Content);
            if (string.IsNullOrEmpty(parsedJson))
            { modelList = JsonConvert.DeserializeObject<List<RFViewModel>>(response.Content); }
            else
            {
                modelList.Add(new RFViewModel()
                { Message = parsedJson });
            }
            return await Task.FromResult(modelList);
        }

        /// <summary>
        ///  if result is empty json is array
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private string ParseJsonArray(string json)
        {
            string result = "";
            var jToken = JToken.Parse(json);
            ////if (jToken is JArray)
            ////{ result=""; }
            ////else 
            if (jToken is JObject)
            { result = jToken["Message"].ToString(); }
            return result;
        }
    }

}
