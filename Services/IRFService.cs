﻿using RF_Data_Importer.Models;
using RF_Data_Importer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RF_Data_Importer.Services
{
    public interface IRFService : IDisposable
    {
        Task Import(RFViewModel entity);
        Task<List<RFViewModel>> Get();
        Task<List<RFViewModel>> GetByDate(DateTime dateFrom, DateTime dateTo);
        Task<List<DoorViewModel>> GetDoors();
        Task ImportDoor(DoorViewModel entity);
        Task<List<RFViewModel>> GetUserWithoutExit(DateTime dateFrom, DateTime dateTo);
    }
}
