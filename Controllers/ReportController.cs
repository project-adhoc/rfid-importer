﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RF_Data_Importer.Models;
using RF_Data_Importer.Services;
using RF_Data_Importer.ViewModels;

namespace RF_Data_Importer.Controllers
{
    public class ReportController : Controller
    {
        private readonly ILogger<ReportController> _logger;
        private readonly IRFService _service;
        public ReportController(ILogger<ReportController> logger, IRFService rfService)
        {
            _logger = logger;
            _service = rfService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(DateTime dateFrom, DateTime dateTo, bool chkUserWithoutExit)
        {
            var date = ValidateDate(dateFrom, dateTo);
            var model = new List<RFViewModel>();
            if (chkUserWithoutExit)
            { model = await GetUserWithoutExit(date.DateFrom, date.DateTo); }
            else { model = await GetByDateRange(date.DateFrom, date.DateTo); }
            ViewBag.FromDate = date.DateFrom.ToString("dd-MMM-yyyy");
            ViewBag.ToDate = date.DateTo.ToString("dd-MMM-yyyy");
            ViewBag.IsUserWithoutExit = chkUserWithoutExit;
            return View(model);
        }
        private async Task<List<RFViewModel>> GetByDateRange(DateTime dateFrom, DateTime dateTo)
        {
            var date = ValidateDate(dateFrom, dateTo);
            var data = await _service.GetByDate(date.DateFrom, date.DateTo);
            return data;
        }
        //[HttpGet]
        private async Task<List<RFViewModel>> GetUserWithoutExit(DateTime dateFrom, DateTime dateTo)
        {
            var date = ValidateDate(dateFrom, dateTo);
            var data = await _service.GetUserWithoutExit(date.DateFrom, date.DateTo);
            return data;
        }

        [HttpGet]
        public async Task<IActionResult> GetDoors()
        {
            var data = await _service.GetDoors();
            return View("~/Views/Report/Doors.cshtml", data);
        }

        private DateRangeModel ValidateDate(DateTime dateFrom, DateTime dateTo)
        {
            var from = dateFrom;
            var to = dateTo.Year.ToString() == "1" ? dateFrom : dateTo;
            if (dateFrom.Year.ToString() == "1" && to.Year.ToString() == "1")
            {
                from = DateTime.Now.AddDays(-1);
                to = DateTime.Now;
            }
            return new DateRangeModel
            {
                DateFrom = from,
                DateTo = to
            };
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}