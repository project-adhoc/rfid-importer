﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RF_Data_Importer.Models;
using RF_Data_Importer.Services;
using RF_Data_Importer.ViewModels;

namespace RF_Data_Importer.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRFService _service;
        public HomeController(ILogger<HomeController> logger, IRFService rfService)
        {
            _logger = logger;
            _service = rfService;
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            var modelList = new List<RFViewModel>();
            var message = "";
            var rowNo = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    if (file != null && file.Length > 0)
                    {
                        if (file.FileName.EndsWith(".txt"))
                        {
                            using (var sr = new StreamReader(file.OpenReadStream()))
                            {
                                //// var columnHeader = sr.ReadLine();//// skip the column header
                                /////read each line
                                string content;
                                while ((content = sr.ReadLine()) != null)
                                {
                                    if (!string.IsNullOrEmpty(content))
                                    {
                                        rowNo++;
                                        string[] values = content.Split(new char[] { ',' });
                                        var date = values[6].Replace("'", "").Replace("\"", "");
                                        var model = new RFViewModel
                                        {
                                            AccessMessage = Convert.ToString(values[0]).Replace("\"", ""),
                                            Door = Convert.ToString(values[1]).Replace("\"", ""),
                                            Direction = Convert.ToString(values[2]).Trim().Replace("\"", ""),
                                            Fullname = $"{Convert.ToString(values[3]).Replace("\"", "")}, {Convert.ToString(values[4]).Replace("\"", "")}",
                                            CardNo = Convert.ToString(values[5]).Trim().Replace("\"", ""),
                                            LocalTime = Convert.ToDateTime(date),
                                            Username = Convert.ToString(values[8]).Trim().Replace("\"", ""),
                                            DateImported = DateTime.Now
                                        };
                                        modelList.Add(model);
                                    }
                                }
                            }
                            //// Process the import
                            rowNo = 0;
                            foreach (var entity in modelList)
                            {
                                rowNo++;
                                await _service.Import(entity);
                            }
                            message = $"Uploaded successfully. <a href='/report/index?dateFrom={modelList[0].LocalTime.ToString("dd-MMM-yyyy")}' id='lnkViewImported''>Click here to view<a/>";
                        }
                    }
                    else
                    { message = "Nothing to import"; }
                }
            }
            catch (Exception ex)
            {
                message = $"Error reading row no->{rowNo} - {ex.Message}";
            }
            ViewBag.Message = message;

            return View();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}