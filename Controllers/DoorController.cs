﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RF_Data_Importer.Models;
using RF_Data_Importer.Services;

namespace RF_Data_Importer.Controllers
{
    public class DoorController : Controller
    {
        private readonly ILogger<DoorController> _logger;
        private readonly IRFService _service;
        public DoorController(ILogger<DoorController> logger, IRFService rfService)
        {
            _logger = logger;
            _service = rfService;
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            //var modelList = new List<DoorModel>();
            //var message = "";
            //var rowNo = 0;
            //try
            //{
            //    if (ModelState.IsValid)
            //    {
            //        if (file != null && file.Length > 0)
            //        {
            //            if (file.FileName.EndsWith(".txt"))
            //            {
            //                using (var sr = new StreamReader(file.OpenReadStream()))
            //                {
            //                    //// var columnHeader = sr.ReadLine();//// skip the column header
            //                    /////read each line
            //                    string content;
            //                    while ((content = sr.ReadLine()) != null)
            //                    {
            //                        if (!string.IsNullOrEmpty(content))
            //                        {
            //                            rowNo++;
            //                            string[] values = content.Split(new char[] { ',' });
            //                            var model = new DoorModel
            //                            {
            //                                Door = Convert.ToString(values[1]).Replace("\"", ""),
            //                            };
            //                            modelList.Add(model);
            //                        }
            //                    }
            //                }
            //                //// Process the import
            //                rowNo = 0;
            //                foreach (var entity in modelList)
            //                {
            //                    rowNo++;
            //                    //await _service.ImportDoor(entity);
            //                }
            //                message = "Uploaded successfully";
            //            }
            //        }
            //        else
            //        { message = "Nothing to import"; }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    message = $"Error reading row no->{rowNo} - {ex.Message}";
            //}
            //ViewBag.Message = message;

            return View();
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}