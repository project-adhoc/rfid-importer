﻿function showDialogBox(id, msgType) {
    /// <summary>msg=message    /// msgType=message-success / message-error / message-warning / message-confirm / message-information    /// </summary>
    $(id).find('.modal-header').removeClass().addClass('modal-header modal-header-border-round ' + msgType);
    $(id).modal("show");
}

// Display the loader while the background process still in progress
function showSpinner(message, blnShow) {
    /// <summary>message=message
    /// blnShow=true / false
    /// </summary>
    if (message === "" || message == null) {
        message = "Please wait...";
    }
    if (blnShow) {
        $('#divSpinner').modal({ keyboard: false });
        $('#divSpinner').modal('show');
        $('#divSpinner').on('shown.bs.modal', function () {
            $('#spanMessage').html(message);
        });
    } else {
        $('#divSpinner').modal('hide');
    }
}